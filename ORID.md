O:
    1.Today, we learned about important concepts of agile development, familiarized ourselves with various processes and their roles, and finally assigned roles according to simulated projects.
    2. According to the concept in yesterday's presentation, an elevator speech was designed for the simulation project.
    3. Using the user union knowledge accumulated by the group in completing the presentation, a preliminary diagram was drawn.
    4. A review was conducted on the learning situation this week, summarizing some executable actions and areas of excellence.
    5. I used Notification to build a team workspace, which is a collaborative space shared by the team.
R:
    full and enjoyable.
I:
    Today, there was not much specific coding work involved, but more focused on team work. By simulating projects to experience the process of agile development, I realized that designing a project requires different roles to be responsible for different tasks.
D:
    I have decided to devote my energy to the project with maximum enthusiasm and learn how to conduct agile development through this simulation project.